package org.example

import com.google.gson.Gson
import java.io.File

data class Todo(
    val text: String,
    var done: Boolean
)

class TodoStore {
    companion object {
        const val FILENAME = "todos.json"
    }

    fun save(todos: List<Todo>) {
        val json = Gson().toJson(todos)
        File(FILENAME).writeText(json)
    }

    fun load(): MutableList<Todo> {
        return if (File(FILENAME).exists()) {
            val json = File(FILENAME).readText()
            Gson().fromJson(json, Array<Todo>::class.java).toMutableList()
        } else {
            mutableListOf<Todo>()
        }
    }
}
