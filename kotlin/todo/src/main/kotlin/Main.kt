package org.example

fun main() {
    val todoStore = TodoStore()
    val todoList = TodoList(todoStore)
    val consoleUi = ConsoleUi(todoList)
    todoList.setConsoleUi(consoleUi)
    consoleUi.run()
}