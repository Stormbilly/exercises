class Engine(private val fuelSystem: FuelSystem) {

    fun start(): Boolean {
        return if (fuelSystem.hasFuel()) {
            fuelSystem.consumeFuel(1)
            true
        } else {
            false
        }
    }

    fun stop() {
        // Logic to stop the engine
    }

    fun getStatus(): String {
        return if (fuelSystem.hasFuel()) {
            "Running"
        } else {
            "Stopped"
        }
    }
}
