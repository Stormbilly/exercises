class FuelSystem(private var fuelLevel: Int) {

    fun hasFuel(): Boolean {
        return fuelLevel > 0
    }

    fun consumeFuel(amount: Int) {
        if (fuelLevel >= amount) {
            fuelLevel -= amount
        }
    }

    fun refuel(amount: Int) {
        fuelLevel += amount
    }

    fun getFuelLevel(): Int {
        return fuelLevel
    }
}
