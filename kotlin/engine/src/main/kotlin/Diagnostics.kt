class Diagnostics(private val engine: Engine) {

    fun runDiagnostics(): String {
        return if (engine.getStatus() == "Running") {
            "All systems operational."
        } else {
            "Engine stopped. Check fuel system."
        }
    }
}
