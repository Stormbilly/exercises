package ccd

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ShortyTests {

    @Test
    fun shorterStringIsReturnedAsIs() {
        assertEquals("A", Shorty.shorten("A", 5))
    }
}