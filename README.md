# Übungen / Exercises

Übungsaufgaben der CCD Akademie GmbH
Please see below for english.

## Aufbau des Repositories

Die oberste Ebene des Repos enthält jeweils ein Verzeichnis für die unterschiedlichen Programmiersprachen. Zum Beispiel:
- __cpp__ - C++
- __csharp__ - C#
- __kotlin__ - Kotlin
- __java__ - Java

Eine Ebene darunter sind die einzelnen Übungsaufgaben abgelegt. Es stehen vermutlich nicht alle Aufgaben in jeder Programmiersprache
zur Verfügung. Wir ergänzen dieses Repo ständig um neue Aufgaben.

Eine Ausnahme bildet folgendes Verzeichnis:
- __design__ - Übungen zum Thema Entwurf. Hier findest du Excalidraw Dateien, die du in https://excalidraw.com öffnen kannst.

## Verwenden des Repositories

Um in einer Schulung eine der Übungsaufgaben zu bearbeiten, musst du das Repo zunächst auf deinen Rechner laden. Dazu gibt es diverse Möglichkeiten:
- Herunterladen als ZIP Datei und dann lokal entpacken
- _git clone_ verwenden, entweder auf der Kommandozeile, in deiner IDE oder einem git Tool wie SourceTree

## Bearbeiten der Übungsaufgaben

Nachdem du das Repo auf deinen Rechner geladen hast, kannst du eine Übungsaufgabe bearbeiten. Dazu öffnest du jeweils eines der Projekte. Die Details
dazu hängen einerseits von der Programmiersprache ab, andererseits von der IDE die du verwendest.

# English

## Structure of the Repository

The top level of the repository contains a directory for each of the different programming languages. For example:
- __cpp__ - C++
- __csharp__ - C#
- __kotlin__ - Kotlin
- __java__ - Java

One level down, you will find the individual exercises. Not all exercises may be available in every programming language. We constantly update this repository with new exercises.

An exception is the following directory:
- __design__ - Exercises on design. Here you will find Excalidraw files that you can open at [Excalidraw](https://excalidraw.com).

## Using the Repository

To work on one of the exercises during a training session, you need to download the repository to your computer first. There are several ways to do this:
- Download as a ZIP file and then unpack it locally
- Use _git clone_, either on the command line, in your IDE, or with a git tool like SourceTree

## Working on the Exercises

Once you have downloaded the repository to your computer, you can start working on an exercise. To do this, open one of the projects. The details depend partly on the programming language and partly on the IDE you are using.
