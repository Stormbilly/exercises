﻿namespace randomstringid
{
    public static class RandomStringId
    {
        const int Length_of_Id = 6;

        public const string LegalCharacters =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        static readonly Random _random = new Random();

        public static string New() {
            var result = new char[Length_of_Id];

            for (int i = 0; i < Length_of_Id; ++i) {
                var charIndex = _random.Next(LegalCharacters.Length);
                result[i] = LegalCharacters[charIndex];
            }

            return new string(result);
        }
    }
}