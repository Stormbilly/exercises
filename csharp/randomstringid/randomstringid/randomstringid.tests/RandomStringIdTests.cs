using NUnit.Framework;

namespace randomstringid.tests
{
    [TestFixture]
    public class RandomStringIdTests
    {
        [Test, Repeat(500)]
        public void Generates_correct_id() {
            var id = RandomStringId.New();

            Assert.That(id.Length, Is.EqualTo(6));
            Assert.That(id.Any(ch => RandomStringId.LegalCharacters.Contains(ch)), Is.True);
        }

        [Test, Repeat(500)]
        public void Generated_ids_are_unique() {
            var list_of_ids = new List<string>();
            for(int i = 0; i < 1000; i++) {
                var id = RandomStringId.New();
                Assert.That(list_of_ids.Contains(id), Is.False);
                list_of_ids.Add(id);
            }
        }
    }
}