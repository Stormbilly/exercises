namespace todo;

public class Todo
{
    public Todo(string text) {
        Text = text;
    }
    
    public string Text { get; set; } = "";

    public bool Done { get; set; }
}