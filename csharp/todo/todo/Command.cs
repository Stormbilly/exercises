namespace todo;

public enum Command
{
    None,
    Add,
    List,
    Done,
    Remove,
    Quit
}