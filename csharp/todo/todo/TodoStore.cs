using System.Text.Json;

namespace todo;

public class TodoStore
{
    private const string Filename = "todos.json";

    public void Save(List<Todo> todos) {
        var json = JsonSerializer.Serialize(todos);
        File.WriteAllText(Filename, json);
    }

    public List<Todo> Load() {
        if (File.Exists(Filename)) {
            var json = File.ReadAllText(Filename);
            return JsonSerializer.Deserialize<List<Todo>>(json) ?? new List<Todo>();
        }

        return [];
    }
}