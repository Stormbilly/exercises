﻿namespace motorbikeservice.Interfaces
{
    public interface IMotorBike
    {
        double CalcServiceBill(params double[] priceList);

        double RideByDay(double speed, double hours);

        double RideByYear(double speed, double hours);

        double GetTotalDistance();

        void PrintBill(string paymentData);

        double Calculate();

        int CalculateCompetitionCounter();
    }
}