using MongoDB.Driver;

namespace integrationtests;

public class MongoDbProviderBase
{
    protected readonly IMongoDatabase _database;

    protected MongoDbProviderBase() {
        var host = Environment.GetEnvironmentVariable("MONGODB") ?? "mongodb://127.0.0.1:27017";
        var dbClient = new MongoClient(host);
        _database = dbClient.GetDatabase("example");
    }
}