﻿using System.Diagnostics.CodeAnalysis;

namespace toDictionary;

public static class StringConvert
{
    public static IDictionary<string, string> ToDictionary(string input)
    {
        ThrowExceptionOnInvalidInput(input);

        var results = new Dictionary<string, string>();

        if (IsInputEmpty(input)) return results;
        
        var settings = SplitIntoSettings(input);

        var pairs = settings.Select(SplitIntoPair).ToList();
        
        ThrowExceptionOnInvalidKey(pairs);
        
        pairs.ForEach(results.AddOrUpdate);

        return results;
    }

    private static void ThrowExceptionOnInvalidInput(string input)
    {
        if (input is null) throw new ArgumentNullException();
    }

    private static void ThrowExceptionOnInvalidKey(IEnumerable<(string Key, string Value)> pairs)
    {
        if (pairs.Any(pair => pair.Key == string.Empty)) throw new ArgumentException("Invalid Empty Key");
    }

    private static bool IsInputEmpty(string input) => string.IsNullOrEmpty(input);

    private static string[] SplitIntoSettings(string input) => input.Split(';', StringSplitOptions.RemoveEmptyEntries);
    
    private static (string Key, string Value) SplitIntoPair(string keyValuePair)
    {
        var position = keyValuePair.IndexOf('=');
        var key = keyValuePair[..position];
        var value = keyValuePair[++position..];
        return (key, value);
    }
    
    private static void AddOrUpdate(this IDictionary<string, string> results, (string Key, string Value) pair)
    {
        if (results.ContainsKey(pair.Key))
        {
            results[pair.Key] = pair.Value;
            return;
        }
        
        results.Add(pair.Key, pair.Value);
    }
}