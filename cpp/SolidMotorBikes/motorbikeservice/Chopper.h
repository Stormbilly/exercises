﻿#pragma once
#include <iostream>
#include "IMotorBike.h"

namespace motorbikeservice {

    class Chopper : public interfaces::IMotorBike {
        double _distance;
    public:
        std::string Name;
        std::string Model;
        double StayingQuality;

        Chopper(const std::string& name, const std::string& model)
            : Name(name), Model(model), _distance(0), StayingQuality(1) {}

        void PrintBill(const std::string& paymentData) override {
            std::cout << paymentData << std::endl;
        }

        double Calculate() override {
            return StayingQuality;
        }

        int CalculateCompetitionCounter() override {
            return 0;
        }

        double RideByDay(double speed, double hours) override {
            return speed * hours;
        }

        double RideByYear(double speed, double hours) override {
            return speed * hours;
        }

        double GetTotalDistance() override {
            return _distance;
        }

        double CalcServiceBill(const double* priceList, int count) override {
            double sum = 0;

            for (int i = 0; i < count; i++) {
                sum += priceList[i];
            }

            return sum;
        }

        double Distance() const {
            return _distance;
        }
        void SetDistance(double distance) {
            _distance = distance;
            StayingQuality = _distance / 10 + 1;
        }
    };
}
