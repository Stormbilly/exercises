#ifndef TODICTIONARY_LIBRARY_H
#define TODICTIONARY_LIBRARY_H

#include <map>
#include <string>

std::map<std::string, std::string> ToDictionary(const std::string& input);

#endif //TODICTIONARY_LIBRARY_H
