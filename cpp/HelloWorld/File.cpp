#include "File.h"
#include <fstream>

void File::AppendAllLines(const std::filesystem::path& filePath, const std::vector<std::string>& lines)
{
	std::ofstream file(filePath, std::ios_base::app);
	for(const auto& line : lines)
		file << line << "\n";
}

std::vector<std::string> File::ReadAllLines(const std::filesystem::path& filePath)
{
	std::vector<std::string> lines;
	std::ifstream file(filePath);
	std::string line;
	while(std::getline(file, line))
		lines.push_back(std::move(line));
	return lines;
}
