#include "Templates.h"

std::string Templates::Get(int frequency) const
{
	switch (frequency)
	{
	case 1: return "Hello, ";
	case 2: return "Welcome back, ";
	default: return "Hello my good friend, ";
	}
}
