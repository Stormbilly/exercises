#pragma once

struct Booking
{
	static Booking FromArgs(const std::vector<std::string>& args, char delim);
	void Print(std::ostream&) const;
	void Load(std::string, char delim);
	void Save(std::ostream&, char delim) const;
	std::string name;
	double amount;
	time_t time;
};
