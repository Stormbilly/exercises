#include <gmock/gmock.h>

#include "key_management_1.h"

using namespace testing;

uint8_t HSM_get_random_byte()
{
   return 5;
}

// dummy example test (no real test)
TEST(key_management_tests, createkey_InByteBuffer_RandomFilled)
{
    uint8_t buffer[] = {1,2,3,4,6};
    key_management::create_key(buffer, sizeof buffer);
    EXPECT_THAT(buffer[0], Eq(5));
    EXPECT_THAT(buffer[1], Eq(5));
    EXPECT_THAT(buffer[2], Eq(5));
    EXPECT_THAT(buffer[3], Eq(5));
    EXPECT_THAT(buffer[4], Eq(5));
}