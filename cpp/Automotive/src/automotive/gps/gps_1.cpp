#include "gps_1.h"
#include "gps_types.h"

#include "crypto_1.h"
#include "crypto_types.h"

#include "key_management_1.h"

namespace gps
{
    Position current_destination;

    Return_status init_crypto_module() {
      crypto::init();
      crypto::Key key = {};
      key_management::create_key(reinterpret_cast<uint8_t*>(&key), sizeof(key));
      if (crypto::set_key(key) == crypto::Return_status::valid_key_provided) {
          crypto::Nonce nonce = {0};
        key_management::create_nonce(reinterpret_cast<uint8_t*>(&nonce), sizeof(nonce));
        if (crypto::set_nonce(nonce) == crypto::Return_status::valid_nonce_provided) {
          return Return_status::Success;
        }
      }
      return Return_status::Failure;
    }

    Return_status set_destination_postition(Position position) {
      current_destination = position;
      return Return_status::Success;
    }

    Position get_destination_position() { return current_destination; }

    Return_status get_current_position(Position *position) {
      uint8_t position_as_bytes[12];
      uint8_t hmac_as_bytes[crypto::HMAC_LENGTH];
      if (Driver_obtain_current_position(position_as_bytes, hmac_as_bytes) == 0) {

          if (crypto::verify_hmac(position_as_bytes, 16, reinterpret_cast<crypto::Hmac *>(hmac_as_bytes)) == crypto::Return_status::valid_hmac) {
          Position pos = {
              (uint8_t)(position_as_bytes[0] << 1) + position_as_bytes[1],
              (uint8_t)(position_as_bytes[2] << 1) + position_as_bytes[3],
              (uint8_t)(position_as_bytes[4] << 1) + position_as_bytes[5],
              (uint8_t)(position_as_bytes[6] << 1) + position_as_bytes[7],
              (uint8_t)(position_as_bytes[8] << 1) + position_as_bytes[9],
              (uint8_t)(position_as_bytes[10] << 1) + position_as_bytes[11]};
          *position = pos;
          return Return_status::Success;
        }
      }
      return Return_status::Failure;
    }
}