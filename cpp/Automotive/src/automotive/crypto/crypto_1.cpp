#include <cstdlib>
#include <cstring>

#include "crypto_1.h"
#include "crypto_2.h"

namespace crypto {
    static State current_state = State::uninitialized;
    static Key current_key = {};
    static Nonce *current_nonce = nullptr;

    void init() {
        std::memset(&current_key, 0, sizeof(current_key));
        if (current_nonce != nullptr) {
            delete current_nonce;
        }
        current_nonce = new Nonce {};
        current_state = State::initialized;
    }

    State get_state() { return current_state; }

    Return_status set_key(Key key) {
        if (verify_key(key) == Return_status::valid_key_provided) {
            current_key = key;
            if (current_state == State::nonce_set) {
                current_state = State::nonce_and_key_set;
            } else {
                current_state = State::key_set;
            }
            return Return_status::valid_key_provided;
        }
        return Return_status::invalid_key_provided;
    }

    Return_status set_nonce(Nonce nonce) {
        if (verify_nonce(&nonce) == Return_status::valid_nonce_provided) {
            if (current_nonce == nullptr) {
                init();
            }
            for (int i = 0; i < NONCE_LENGTH; i++) {
                current_nonce->nonce[i] = nonce.nonce[i];
            }
            current_nonce->time_of_creation = nonce.time_of_creation;
            if (current_state == State::key_set) {
                current_state = State::nonce_and_key_set;
            } else {
                current_state = State::nonce_set;
            }
            return Return_status::valid_nonce_provided;
        }
        return Return_status::invalid_nonce_provided;
    }

    Return_status calculate_hmac(const uint8_t *message, int len, Hmac *hmac) {
        if (current_state == State::nonce_and_key_set) {
            if (current_nonce != nullptr) {
                if (third_party_library_calc_hmac(message, len, current_key.key, current_nonce->nonce, hmac->hmac) == 0) {
                    // Delete nonce to make sure it is only used once
                    delete current_nonce;
                    current_nonce = nullptr;
                    return Return_status::hmac_successfully_calculated;
                }
            }
            return Return_status::error_during_hmac_calculation;
        }
        return Return_status::wrong_state;
    }

    Return_status verify_hmac(const uint8_t *message, int len, Hmac *hmac) {
        Hmac own_hmac;
        auto hmac_calc_status = calculate_hmac(message, len, &own_hmac);
        if (hmac_calc_status != Return_status::hmac_successfully_calculated) {
            return hmac_calc_status;
        }
        for (int i = 0; i < HMAC_LENGTH; i++) {
            if (own_hmac.hmac[i] != hmac->hmac[i]) {
                return Return_status::invalid_hmac;
            }
        }
        return Return_status::valid_hmac;
    }
}